package games.wintergame;

import java.util.Random;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflake {
	private double x,y;
	private int size;
	private double speed;
	private Random r;
	
	
	public Snowflake(int size, double speed) {
		super();
		r = new Random();
		this.x = r.nextInt(800);
		this.y = r.nextInt(600) *-1;
		this.size = size;
		this.speed = speed;
	}
	public void update(GameContainer gc, int delta) {
		this.y+=speed;
		if (this.y == 600) {
			this.y = 0;
			this.x = r.nextInt(800);
			
			
		}
		
	}
	public void render(Graphics graphics) {
		graphics.fillOval((float)this.x, (float)this.y, this.size, this.size);
	}	
}
