package games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	private Snowflake s1;
	private List<Snowflake> snowflakes = new ArrayList<>();
	

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		for (Snowflake sf : snowflakes) {
		      sf.render(graphics);
		    }
		
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		for (int i = 0; i <= 50; i++) {
			Snowflake small = new Snowflake(5, 0.25);
			Snowflake medium = new Snowflake(10, 0.5);
			Snowflake big = new Snowflake(15, 0.75);
			snowflakes.add(small);
			snowflakes.add(medium);
			snowflakes.add(big);
	
		}
		
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Snowflake sf : snowflakes) {
		      sf.update(gc, delta);
		    }
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
